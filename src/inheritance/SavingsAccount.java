package inheritance;
public class SavingsAccount extends BankAccount {
	private boolean status;
        final double serviceCharge = 1;
	private String accountNr;
	
	// constructor
	public SavingsAccount (double balance, double intRate) {
		super(balance, intRate);
		if (balance >= 25) { this.status = true; }
	}
	
	/*** withdraw
	 * Determines whether the account is 
	 * inactive before a withdraw is made
	 * @param withdraw
	 */
	public void withdraw (double withdraw) {
		double theBalance;
		if (this.status == true) {
			theBalance = super.getBalance() - withdraw;
			if (theBalance < 25) { this.status = false; }
			super.withdraw(withdraw);
		}
	}
	
	/*** deposit
	 * Determines whether the account is 
	 * inactive before a deposit is made
	 * If inactive and the deposit brings the balance
	 * over 25, this.status = true
	 * @param deposit
	 */
	public void deposit (double deposit) {
		double theBalance = super.getBalance() + deposit;
		if (theBalance >= 25) { this.status = true; }
		super.deposit(deposit);
	}
	
	/*** monthlyProcess
	 * Checks # of withdrawals, if # > 4 a service charge of $1
	 * is added for each withdrawal above 4.
	 * If balance drops below 25, this.status = false 
	 */
   
	public void monthlyProcess () {
		double theBalance;
		if (super.getNrWithdrawals() > 4) {
			theBalance = super.getBalance() - (super.getNrWithdrawals() - 4);
			if (theBalance < 25) { this.status = false; }
			super.setSrvCharge((super.getNrWithdrawals() - 4)*serviceCharge);
			super.monthlyProcess();
		}
		
	}

	/**
	 * @param accountNr the accountNr to set
	 */
	public void setAccountNr(String accountNr) {
		this.accountNr = accountNr;
	}

	/**
	 * @return the accountNr
	 */
	public String getAccountNr() {
		return accountNr;
	}

	@Override
	public String toString() {
		return accountNr + "," + getBalance() + "," + getNrDeposit() + "," + getNrWithdrawals()
				+ "," + getIntRate() + "," + getSrvCharge();
	}

	public String checkStatus() {
		if (status)
			return "active";
		else
			return "inactive";
	}
}

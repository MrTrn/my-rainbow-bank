package inheritance;
import javax.swing.*;
import java.awt.*;


@SuppressWarnings("serial")
public class MyHeaderPicture extends JPanel{
	public void paintComponent(Graphics g) {
		Image image = new ImageIcon("header.jpg").getImage();
		g.drawImage(image,0,0,this);
	}
}

package inheritance;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;


public class BankApplication {
	
	private int activeAccount; // holds the selected account
        private int nrAccounts=0; // holds the number of accounts
	// The file w/account info
	private File theFile = new File("input.txt");
	private SavingsAccount[] arrayAccounts;
	// Frame
	private JFrame frame;
	// Panels:
	private JPanel panelHead;
	private JPanel panelLeft;
	private JPanel panelCenter;
	private JPanel panelRight;
	// Labels: (lpl = label panel left, lpc = label panel center)
	private JLabel label;
	private JLabel label2;
	private JLabel lplAccount;
	private JLabel lpcSelected;
	private JLabel lpcStatus;
	private JLabel lpcBalance;
	private JLabel lpcDeposits;
	private JLabel lpcWithdrawals;
	private JLabel lpcAnnualRate;
	private JLabel lpcMntCharge;
	private JLabel lprDeposit;
	private JLabel lprWithdraw;
	// TextFields:
	private JTextField txtDeposit;
	private JTextField txtWithdrawal;
	// Buttons:
	private JButton buttonDep;
	private JButton buttonWit;
	private JButton buttonSimulate;
	//private JButton buttonSaveFile;
	private JRadioButton[] arrayRadio;

	// >Group the radio buttons.
	private ButtonGroup group = new ButtonGroup();
		
	public static void main (String[] args) {
		BankApplication gui = new BankApplication ();
		gui.go();
	}

	public void go() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // make sure we close
		
		panelHead = new JPanel();
		panelLeft = new JPanel();
		panelCenter = new JPanel();
		panelRight = new JPanel();
		
		label = new JLabel(" "); // Only for decoration
		label2 = new JLabel(" "); // Only for decoration
		
		// make the header picture
		MyHeaderPicture headerImage = new MyHeaderPicture();
		headerImage.setSize(600, 100);
		
		// panelHead
		panelHead.setLayout(new BoxLayout(panelHead, BoxLayout.Y_AXIS));
		panelHead.setPreferredSize(new Dimension(600,100));
		panelHead.add(headerImage);
		
		// panelLeft
		lplAccount = new JLabel();
		lplAccount.setText("  Accounts:");
		panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.Y_AXIS));
		panelLeft.add(lplAccount);
		//panelLeft.add(radio1);
		//panelLeft.add(radio2);
		
		// panelCenter
		 lpcSelected = new JLabel(" ");
		 lpcStatus = new JLabel("   Please select account on left side.");
		 lpcBalance = new JLabel("   Use tools on right side to manipulate");
		 lpcDeposits = new JLabel("   accounts.");
		 lpcWithdrawals = new JLabel("   'Simulate'-button simulates monthly routine");
		 lpcAnnualRate = new JLabel("");
		 lpcMntCharge = new JLabel();
		 panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.Y_AXIS));
		 panelCenter.add(lpcSelected);
		 panelCenter.add(lpcStatus);
		 panelCenter.add(lpcBalance);
		 panelCenter.add(lpcDeposits);
		 panelCenter.add(lpcWithdrawals);
		 panelCenter.add(lpcAnnualRate);
		 panelCenter.add(lpcMntCharge);
		 
		 // panelRight
		 lprDeposit = new JLabel("Deposit: ");
		 lprWithdraw = new JLabel("Withdraw: ");
		 txtDeposit = new JTextField(1);
		 txtDeposit.setPreferredSize(new Dimension(100,20));
		 txtDeposit.setMaximumSize(new Dimension(100,20));
		 txtWithdrawal = new JTextField(1);
		 txtWithdrawal.setPreferredSize(new Dimension(100,20));
		 txtWithdrawal.setMaximumSize(new Dimension(100, 20));
		 buttonDep = new JButton ("OK");
                 buttonDep.setEnabled(false);
		 buttonWit = new JButton ("OK");
                 buttonWit.setEnabled(false);
		 buttonSimulate = new JButton ("Simulate");
                 buttonSimulate.setEnabled(false);
		 //buttonSaveFile = new JButton ("Save File");
                 
		 panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.Y_AXIS));
		 panelRight.setMaximumSize(new Dimension(100,100));
		 panelRight.setPreferredSize(new Dimension(100,100));
		 panelRight.add(lprDeposit);
		 panelRight.add(txtDeposit);
		 panelRight.add(buttonDep);
		 panelRight.add(label);
		 panelRight.add(lprWithdraw);
		 panelRight.add(txtWithdrawal);
		 panelRight.add(buttonWit);
		 panelRight.add(label2);
		 panelRight.add(buttonSimulate);
		 //panelRight.add(buttonSaveFile);
		 /* listeners for panelRight */
		 buttonDep.addActionListener(new ButtonDepositListener());
		 buttonWit.addActionListener(new ButtonWithdrawalListener());
		 buttonSimulate.addActionListener(new ButtonSimulateListener());
		 
                 // read the file and handle the info from file
		 ReadTheFile();
		 
		frame.setTitle("Saving accounts");
		frame.getContentPane().add(BorderLayout.NORTH,panelHead);
		//frame.getContentPane().add(BorderLayout.SOUTH, colorButton);
		frame.getContentPane().add(BorderLayout.WEST, panelLeft);
		frame.getContentPane().add(BorderLayout.EAST, panelRight);
		frame.getContentPane().add(BorderLayout.CENTER, panelCenter);
		
		frame.setMinimumSize(new Dimension(490,400));
		frame.setSize(490,400); // Set window size
		frame.setVisible(true);
		
	}
	class LabelListener implements ActionListener {
        @Override
		public void actionPerformed(ActionEvent event) {
			label.setText(event.getActionCommand());
		}
	}

	class RadioListener implements ActionListener {
        @Override
		public void actionPerformed(ActionEvent event) {
			//label.setText(event.getActionCommand()); //debug
			int i=0;
                        int over = 0;
			String e = event.getActionCommand();
			do {
				if (arrayRadio[i].getText() == e) {
					activeAccount = i;
                                        over = 1;
					updateLabels();
				}
				i++;
			} while ((over ==0) );
			
                // enables the buttons
                 buttonDep.setEnabled(true);
                 buttonWit.setEnabled(true);
                 buttonSimulate.setEnabled(true); 
		}
	}
        
        /*** updateLabels
         * Updates the labels on screen
         */
        private void updateLabels() {
             lpcSelected.setText("Selected Account: " + arrayAccounts[activeAccount].getAccountNr());
             lpcStatus.setText("Status: " + arrayAccounts[activeAccount].checkStatus());
             lpcBalance.setText("Balance: " + arrayAccounts[activeAccount].getBalance());
             lpcDeposits.setText("Deposits: " + arrayAccounts[activeAccount].getNrDeposit());
             lpcWithdrawals.setText("Withdrawals: " + arrayAccounts[activeAccount].getNrWithdrawals());
             lpcAnnualRate.setText("AnnualRate: " + arrayAccounts[activeAccount].getIntRate());
             lpcMntCharge.setText("Monthly Charge: " + arrayAccounts[activeAccount].getSrvCharge());
	}
        
	/*** ButtonDepositListener
	 * Handles whenever someone clicks on the deposit button
	 * @author rene
	 *
	 */
	class ButtonDepositListener implements ActionListener {
        @Override
		public void actionPerformed(ActionEvent event) {
            
                    if ((txtDeposit.getText() != null) && (txtDeposit.getText() != "")) {
			Double fromString = Double.parseDouble(txtDeposit.getText());
                        //debug:
			//JOptionPane.showMessageDialog(null,arrayAccounts[activeAccount].deposit(fromString));
			// deposit what's in the box
                        arrayAccounts[activeAccount].deposit(fromString);
                    }
                    // updates labels on screen
			updateLabels();	
		}

	}
        /*** ButtonWithrawalListener
         * Handles when the withdrawal button is clicked
         */
	public class ButtonWithdrawalListener implements ActionListener {
        @Override
		public void actionPerformed(ActionEvent event) {
                    if ((txtWithdrawal.getText() != null) && (txtWithdrawal.getText() != "")) {
			Double fromString = Double.parseDouble(txtWithdrawal.getText());
                        //debug:
			//JOptionPane.showMessageDialog(null,arrayAccounts[activeAccount].deposit(fromString));
			// deposit what's in the box
                        arrayAccounts[activeAccount].withdraw(fromString);
                    }
                    // updates labels on screen
			updateLabels();	
		}
	}
        /*** ButtonSimulateListener
         * Handles when Simulate button is clicked
         */
	private class ButtonSimulateListener implements ActionListener {
        @Override
                public void actionPerformed(ActionEvent event) {
                    
                        arrayAccounts[activeAccount].monthlyProcess();
      
                    // updates labels on screen
			updateLabels();	
		}
        }
	
	/*** ReadTheFile
	 * Handles the Read from file stuff
	 * The filename is preset in 'theFile'
	 */
	public void ReadTheFile() {
		int i = 0;
		try {
			FileReader fileReader;
			fileReader = new FileReader(theFile);
		
			BufferedReader reader = new BufferedReader(fileReader);
			
			String line = null;
			int test;
			
			//making SavingsAccount arrays and RadioButton arrays
			arrayAccounts = new SavingsAccount[50];
			arrayRadio = new JRadioButton[50];
			
			// until we meet an empty line
			while ((line = reader.readLine()) != null) {
				// check that we what fillArray demands
				if ((test = line.split(",").length) == 6) {
					fillArray(i,line);
					i++;
				}
			}
			// Close file
			reader.close();
		} 
		catch (FileNotFoundException e) {
			// Couldn't find file. Give error message 
			JOptionPane.showMessageDialog(null,"The file is not found!");
		} 
		catch (IOException e) {
			// Other error: Give error message 
			JOptionPane.showMessageDialog(null,"Error: " + e.getMessage());
		}
		
	
}
	/*** fillArray
	 * Fills the arrays, Both the account and radio button
	 * Adds RadioButtons to panelLeft on the fly
	 * and to the RadioButton group
	 * @param i
	 * @param readLine
	 */
	private void fillArray(int i, String readLine) {
		// arrayAccounts arrayRadio
		/* from file:
		 * part[0] account#
		 * part[1] balance
		 * part[2] deposits
		 * part[3] withdrawals
		 * part[4] annual rate
		 * part[5] monthly charge
		 */
		// Split given string in readLine
		String [] part = readLine.split(",");
		
		// Make new SavingsAccount in array
		arrayAccounts[i] = new SavingsAccount(Double.parseDouble(part[1]),Double.parseDouble(part[4]));
		
		// Set Parameters
		arrayAccounts[i].setAccountNr(part[0]);
		arrayAccounts[i].setNrDeposit(Integer.parseInt(part[2]));
		arrayAccounts[i].setNrWithdrawals(Integer.parseInt(part[3]));
		arrayAccounts[i].setSrvCharge(Double.parseDouble(part[5]));
		arrayRadio[i] = new JRadioButton(part[0]);
		arrayRadio[i].addActionListener(new RadioListener());
		
		// add RadioButton to panelLeft
		panelLeft.add(arrayRadio[i]);
		// add RadioButton to RadioButton group
		group.add(arrayRadio[i]);
                nrAccounts++;
		
	}	
}
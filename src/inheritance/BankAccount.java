package inheritance;
public abstract class BankAccount {
	private double balance;
	private int nrDeposit; // Numbers of deposits this month
	private int nrWithdrawals; // Number of withdrawals
	private double intRate; // Annual interest rate
	private double srvCharge; // Monthly service charge
	
	public BankAccount(double balance, double intRate) {
		this.balance = balance;
		this.intRate = intRate;
	}
	
	/*** Deposit
	 * adds argument to balance
	 * incs nrDeposits
	 * @param deposit
	 */
	public void deposit (double deposit) {
		this.balance += deposit;
		this.nrDeposit++;
	}
	
	/*** Withdrawal
	 * subtracts argument from balance
	 * incs nrWithdrawals
	 * @param withdraw
	 */
	public void withdraw (double withdraw){
		this.balance -= withdraw;
		this.nrWithdrawals++;
	}
	/*** calcInterest
	 * Updates the balance by calculating and adding the
	 * monthly interest earned by the account 
	 */
	public void calcInterest () {
		double mntIntRate; // Monthly interest rate
		double mntInt; // Monthly interests
		mntIntRate = this.intRate/12;
		mntInt = this.balance * mntIntRate;
		this.balance += mntInt;		
	}
	
	/*** monthlyProcess
	 * subtracts the monthly service charge from balance
	 * calls calcInterest()
	 * resets nrDeposit, nrWithdrawals, srvCharge to zero
	 */
	public void monthlyProcess () {
		this.balance = this.balance - this.srvCharge;
		this.calcInterest();
		this.nrDeposit = 0;
		this.nrWithdrawals = 0;
		this.srvCharge = 0;
	}
	
	// getters and setters:
	public double getBalance() {
		return balance;
	}

	public int getNrDeposit() {
		return nrDeposit;
	}

	public void setNrDeposit(int nrDeposit) {
		this.nrDeposit = nrDeposit;
	}

	public int getNrWithdrawals() {
		return nrWithdrawals;
	}

	public void setNrWithdrawals(int nrWithdrawals) {
		this.nrWithdrawals = nrWithdrawals;
	}

	public double getIntRate() {
		return intRate;
	}

	public void setIntRate(double intRate) {
		this.intRate = intRate;
	}

	public double getSrvCharge() {
		return srvCharge;
	}

	public void setSrvCharge(double srvCharge) {
		this.srvCharge = srvCharge;
	}
}
